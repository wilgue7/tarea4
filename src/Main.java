import javax.smartcardio.CardTerminal;

public class Main {

    public static void main(String[] args) {
        /**
         * Your application should construct one instance of each of the following classes
         * Object
         * Fruit
         * Apple
         * Citrus
         * Orange
         */
        Object obj = new Object();
        Apple apple=new Apple();
        Citrus citrus =new Citrus();
        Fruit fruit=new Fruit();
        Orange orange= new Orange();
        Sqeezable s = null;
        /**
         * Try to cast each of these objects to each of the following types:
         * Fruit
         * Apple
         * Squeezable
         * Citrus
         * Orange
         */
        /**
         * Citrus:Orange, Orange:Citrus
         */
        try {
            Citrus cit = new Citrus();
            cit = orange;
            System.out.println("Citrus:Orange="+"ok");
        }catch (Exception ex){
            System.out.println("Citrus:Orange="+"No");
        }

        try {
            Citrus cit2 = new Citrus();
            orange= (Orange) cit2;
            System.out.println("Orange:Citrus="+"ok");
        }catch (Exception ex){
            System.out.println("Orange:Citrus="+"No");
        }

        /**
         * Citrus:Apple, Apple:Citrus
         */
        try {
            Citrus cit = new Citrus();
            // cit = apple;
            System.out.println("Citrus:Apple="+"NO");
        }catch (Exception ex){
            System.out.println("Citrus:Apple="+"No");
        }

        try {
            Citrus cit2 = new Citrus();
            // apple= (Apple)cit2;
            System.out.println("Apple:Citrus="+"NO");
        }catch (Exception ex){
            System.out.println("Apple:Citrus="+"No");
        }
        /**
         * Citrus:Fruit, Fruit:Citrus
         */
        try {
            Citrus cit3 = new Citrus();
          //  cit3 = fruit;                 error compilacion
            System.out.println("Citrus:Fruit="+"No");
        }catch (Exception ex){
            System.out.println("Citrus:Fruit="+"No");
        }

        try {
            Fruit f = new Fruit();
              f = (Citrus)citrus;
            System.out.println("Fruit:Citrust="+"Si");
        }catch (Exception ex){
            System.out.println("Fruit:Citrus="+"No");
        }
    /**
     * citrus:squezable, squezable:citrus
     */

        try {
            Citrus c4 = new Citrus();
            // c4 = (Sqeezable)s;
            System.out.println("Citrus:Sqeezable="+"No");
        }catch (Exception ex){
            System.out.println("Citrus:Sqeezable="+"No");
        }

        try {
            s = (Citrus)citrus;
            System.out.println("Sqeezable:Citrus="+"Si");
        }catch (Exception ex){
            System.out.println("Sqeezable:Citrus="+"No");
        }



    /**
     * apple:Squeezable ,Sqeezable:Apple
     */
        try{
            Apple ap=new Apple();
            Sqeezable sq =(Sqeezable) ap;
            System.out.println("Apple:Squeezable=SI");
        }catch (ClassCastException e){
            System.out.println("Apple:Squeezable=NO");
        }

        try{
            Apple ap2=new Apple();
            Sqeezable sq2 ;
            //sq2=(Apple)ap2;       error compilacion
            System.out.println("Squeezable:Apple=No");
        }catch (ClassCastException e){
            System.out.println("Squeezable:Apple=NO");
        }

        /**
         * Apple:Fruit, Fruit:Apple
         */
        try{
            Apple App = new Apple();
            Fruit fr= (Fruit)App;
            System.out.println("Apple:Fruit=SI");
        }catch (ClassCastException e){
            System.out.println("Apple:Fruit=NO");
        }


        try{
            Apple Ap3 = new Apple();
          // Ap3=(Fruit)fruit;          error compilacion
            System.out.println("Fruit:Apple:=No");
        }catch (ClassCastException e){
            System.out.println("Fruit:Apple=NO");
        }
        /**
         * Apple:Orange,Orange:Apple
         */
        try{
            Orange or=new Orange();
            //apple=(Orange)orange; error compilacion
            System.out.println("Apple:Orange=No");
        }catch (ClassCastException e){
            System.out.println("Apple:Orange=NO");
        }
        try{
            Orange or=new Orange();
           // or=(Apple)apple;      error compilacion
            System.out.println("Orange:Applee=No");
        }catch (ClassCastException e){
            System.out.println("Orange:Apple=NO");
        }


/**
 * Orange:Fruit
 */
        try{
            Orange or=new Orange();
            Fruit fr =(Fruit)or;
            System.out.println("Orange:Fruit=SI");
        }catch (ClassCastException e){
            System.out.println("Orange:Fruit=NO");
        }
        /**
         * Fruit: Squeezable
         */
        try{
            Fruit fr = new Fruit();
            Sqeezable sq=(Sqeezable) fr;
            System.out.println("Fruit: Squeezable=SI");
        }catch (ClassCastException e){

            System.out.println("Fruit: Squeezable=NO");
        }

    }
}
